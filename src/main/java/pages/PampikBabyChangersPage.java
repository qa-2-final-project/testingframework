package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class PampikBabyChangersPage extends BasePage {

    public PampikBabyChangersPage(WebDriver driver) {
        super(driver);
    }

    private static class Locators {
        private final static By login = By.xpath("//span[text()='Увійти']");
        private final static By loginPhone = By.cssSelector("[name='Customer[login_phone]']");
        private final static By customerPassword = By.cssSelector("[name='Customer[customer_password]']");
        private final static By submitLogin = By.cssSelector("#submit-login");
        private final static By title = By.xpath("//h1[contains(text(), 'Доброго дня')]");
        private final static By diapersAndChanging = By.xpath("//span[contains(text(), 'Підгузки і сповивання')]");
        private final static By sorterTrigger = By.cssSelector(".sorter__trigger-val");
        private final static By dataSortingByName = By.cssSelector("[data-sort='name']");
        private final static By changingMattress = By.cssSelector("[data-key='720607']");
        private final static By changingMattressWishlistButton = By.xpath("//input[@value='720607']/..");
        private final static By addToWishListButton = By.cssSelector(".cst-btn.popup-form__cst-btn.cst-btn--shadow2");
        private final static By wishListCounter = By.cssSelector("#fave-link-counter");
        private final static By changingMattressInWishlist = By.cssSelector(".new-product-item__name");
        private final static By WishListRemoveButton = By.cssSelector(".close.remove-item");
        private final static By changingMattressCartButton = By.cssSelector("[data-btn-product-id='720607']");
        private final static By cartCounter = By.
                xpath("/html/body/div[2]/header/div[2]/div[2]/div/div[2]/div/a[1]/span[1]/span");
        private final static By clearCartButton = By.cssSelector(".cart__clear.clear-cart_");
        private final static By removeAllItems = By.cssSelector(".cart-remove__icon.icon.icon-cross");
        private final static By miniCartItem = By.cssSelector(".item-title");
        private final static By moveAllItemsToWishlistAndClear = By.cssSelector(".icon.icon-d-heaet.in-wishlist__icon");
        private final static By makeOut = By.cssSelector(".cst-btn.hidden-960");
        private final static By orderTitle = By.cssSelector(".order__title");
        private final static By placeAnOrder = By.cssSelector(".styled__ScButton-sc-1j7qfdy-0.dCSgov.btn");
        private final static By placingAnOrder = By.cssSelector(".styled__CheckoutPageSC-sc-1rha2zy-0.hsvpcf>h3");
        private final static By editOrder = By.xpath("//span[text()='Редагувати']");
        private final static By itemContentDelete = By.cssSelector(".styled__ProductCardItemContentDeleteControlSC-sc4824-3.Gnxfe");
        private final static By removeOrder = By.xpath("//span[text()='Вилучити']");
        private final static By backToShopping = By.xpath("//span[text()='Повернутися до покупок']");
    }

    private static class Labels {
        private final static String url = "https://pampik.com/ua/category/pelenatoryi-i-matrasiki-dlya-pelenaniya";
        private final static String pampikLogin = "0934761939";
        private final static String pampikPassword = "ZM2xuE8whzsZX6N";
        private final static String babyChangers = "//a[contains(text(), 'Пеленатори і матрацики для сповивання')]";
        private final static String changingMattressText = "М'який пеленальний матрац Baby Veres Elephant pink, 72х80 см (421.2)";
        private final static String placingAnOrderText = "Оформлення замовлення №";
    }

    public void openPage() {
        driver.get(Labels.url);
    }

    public void clickLoginButton() {
        elements.clickOnElement(Locators.login);
    }

    public void accountLogin() {
        elements.inputData(Locators.loginPhone, Labels.pampikLogin);
        elements.inputData(Locators.customerPassword, Labels.pampikPassword);
        elements.clickOnElement(Locators.submitLogin);
        elements.findElement(Locators.title);
    }

    public void sortingByName() {
        elements.clickOnElement(Locators.sorterTrigger);
        elements.clickOnElement(Locators.dataSortingByName);
        elements.findElement(Locators.changingMattress);
    }

    public void windowScrollToWishListCounter() {
        elements.windowScrollToElement(Locators.wishListCounter);
    }

    public void clickDiapersAndChangingSubmenuBabyChangersButtonByXpath() {
        elements.waitSomeTime(500);
        action.moveToElement(elements.findElement(Locators.diapersAndChanging));
        elements.clickOnElementByXpath(Labels.babyChangers);
        elements.findElement(Locators.sorterTrigger);
    }

    public void moveToItemAndClickWishlistButton() {
        action.moveToElement(elements.findElement(Locators.changingMattress));
        elements.clickOnElement(Locators.changingMattressWishlistButton);
        elements.clickOnElement(Locators.addToWishListButton);
    }

    public void wishlistCounterCheck() {
        assertions.equalsOfText(elements.getElementText(Locators.wishListCounter), "1");
    }

    public void clickOnWishListButton() {
        elements.clickOnElement(Locators.wishListCounter);
    }

    public void wishlistPageCheck() {
        assertions.equalsOfText(elements.getElementText(Locators.changingMattressInWishlist), Labels.changingMattressText);
    }

    public void deleteItemFromWishlist() {
        elements.clickOnElement(Locators.WishListRemoveButton);
    }

    public void moveToItemAndClickCartButton() {
        action.moveToElement(elements.findElement(Locators.changingMattress));
        elements.waitSomeTime(500);
        elements.clickOnElement(Locators.changingMattressCartButton);
    }

    public void windowScrollToCartCounter() {
        elements.waitSomeTime(2000);
        elements.windowScrollToElement(Locators.cartCounter);
    }

    public void cartCounterCheck() {
        assertions.equalsOfText(elements.getElementText(Locators.cartCounter), "1");
    }

    public void cartPageCheck() {
        assertions.equalsOfText(elements.getElementText(Locators.miniCartItem), Labels.changingMattressText);
    }

    public void moveToCartButton() {
        action.moveToElement(elements.findElement(Locators.cartCounter));
    }

    public void moveToCartButtonAndRemoveAllItems() {
        action.moveToElement(elements.findElement(Locators.cartCounter));
        elements.clickOnElement(Locators.clearCartButton);
        elements.clickOnElement(Locators.removeAllItems);
    }

    public void moveToCartButtonAndMoveAllItemsToWishlistAndClear() {
        action.moveToElement(elements.findElement(Locators.cartCounter));
        elements.clickOnElement(Locators.clearCartButton);
        elements.clickOnElement(Locators.moveAllItemsToWishlistAndClear);
        elements.clickOnElement(Locators.addToWishListButton);
        elements.waitSomeTime(1000);
    }

    public void clickMakeOut() {
        elements.clickOnElement(Locators.makeOut);
    }

    public void clickPlaceAnOrder() {
        elements.findElement(Locators.orderTitle);
        elements.clickOnElement(Locators.placeAnOrder);
    }

    public void orderPageCheck() {
        assertions.equalsOfText(elements.getElementText(Locators.placingAnOrder), Labels.placingAnOrderText);
    }

    public void clickEditOrder() {
        elements.clickOnElement(Locators.editOrder);
    }

    public void clickRemoveOrder() {
        elements.clickOnElement(Locators.itemContentDelete);
        elements.clickOnElement(Locators.removeOrder);
    }

    public void clickBackToShopping() {
        elements.clickOnElement(Locators.backToShopping);
    }
}