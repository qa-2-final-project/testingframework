package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.Objects;

public class PampikRegisterPage extends BasePage {

    public PampikRegisterPage(WebDriver driver) {
        super(driver);
    }

    private static class Locators {
        private final static By name = By.cssSelector("[placeholder=\"Ваше ім'я*\"]");
        private final static By email = By.cssSelector("[placeholder='E-mail*']");
        private final static By phone = By.cssSelector("[placeholder='Телефон*']");
        private final static By password = By.cssSelector("[placeholder='Пароль*']");
        private final static By city = By.cssSelector("[placeholder='Місто*']");
        private final static By checkbox = By.cssSelector("[for='qw2']");
        private final static By registrationButton = By.cssSelector("#registr-btn");
        private final static By fillInFieldXpath = By.xpath("//div[text()='Заповніть поле']");
        private final static By notContainCharactersXpath = By.xpath("//div[contains(text(), 'не повинно містити символи')]");
        private final static By enterValidEmailXpath = By.xpath("//div[text()='Введіть правильний email']");
        private final static By enterValidPhoneXpath = By.xpath("//div[text()='Введіть правильний телефон']");
        private final static By startEnteringCityXpath = By.xpath("//div[text()='Почніть вводити в поле місто і виберіть місто зі списку']");
        private final static By markItXpath = By.xpath("//div[text()='Відзначте']");
        private final static By userAlreadyExistsXpath = By.xpath("//div[text()='Користувач вже є у нашій базі']");
    }

    private static class Labels {
        private final static String url = "https://pampik.com/ua/account/register";
        private final static String nameTest = "Василь Тестенко";
        private final static String nameRegistered = "Андрій Погановський";
        private final static String emailTest = "vasyl@testenko.com";
        private final static String emailRegistered = "poganovsky@gmail.com";
        private final static String phoneTest = "0931112233";
        private final static String phoneRegistered = "0934761939";
        private final static String passwordTest = "2222222222";
        private final static String passwordRegistered = "ZM2xuE8whzsZX6N";
        private final static String cityTestRegistered = "Київ";
        private final static String fillInFieldText = "Заповніть поле";
        private final static String notContainCharactersText = "Поле \"Ім'я\" не повинно містити символи";
        private final static String enterValidEmailText = "Введіть правильний email";
        private final static String enterValidPhoneText = "Введіть правильний телефон";
        private final static String emptyField = "";
        private final static String startEnteringCityText = "Почніть вводити в поле місто і виберіть місто зі списку";
        private final static String markItText = "Відзначте";
        private final static String userAlreadyExistsText = "Користувач вже є у нашій базі";
    }

    public void openPage() {
        driver.get(Labels.url);
    }

    public void nameFieldTest(String input) {
        elements.inputData(Locators.name, input);
        elements.inputData(Locators.email, Labels.emailTest);
        elements.inputData(Locators.phone, Labels.phoneTest);
        elements.inputData(Locators.password, Labels.passwordTest);
        elements.inputData(Locators.city, Labels.cityTestRegistered);
        elements.clickOnElement(Locators.checkbox);
        elements.clickOnElement(Locators.registrationButton);
    }

    public void nameFieldCheck(String input) {
        if (Objects.equals(input, "")) {
            assertions.equalsOfText(elements.getElementText(Locators.fillInFieldXpath), Labels.fillInFieldText);
        } else {
            assertions.equalsOfText(elements.getElementText(Locators.notContainCharactersXpath), Labels.notContainCharactersText);
        }
    }

    public void emailFieldTest(String input) {
        elements.inputData(Locators.name, Labels.nameTest);
        elements.inputData(Locators.email, input);
        elements.inputData(Locators.phone, Labels.phoneTest);
        elements.inputData(Locators.password, Labels.passwordTest);
        elements.inputData(Locators.city, Labels.cityTestRegistered);
        elements.clickOnElement(Locators.checkbox);
        elements.clickOnElement(Locators.registrationButton);
    }

    public void emailFieldCheck() {
        assertions.equalsOfText(elements.getElementText(Locators.enterValidEmailXpath), Labels.enterValidEmailText);
    }

    public void phoneFieldTest(String input) {
        elements.inputData(Locators.name, Labels.nameTest);
        elements.inputData(Locators.email, Labels.emailTest);
        elements.inputData(Locators.phone, input);
        elements.inputData(Locators.password, Labels.passwordTest);
        elements.inputData(Locators.city, Labels.cityTestRegistered);
        elements.clickOnElement(Locators.checkbox);
        elements.clickOnElement(Locators.registrationButton);
    }

    public void phoneFieldCheck(String input) {
        if (Objects.equals(input, "")) {
            assertions.equalsOfText(elements.getElementText(Locators.fillInFieldXpath), Labels.fillInFieldText);
        } else {
            assertions.equalsOfText(elements.getElementText(Locators.enterValidPhoneXpath), Labels.enterValidPhoneText);
        }
    }

    public void passwordFieldTest() {
        elements.inputData(Locators.name, Labels.nameTest);
        elements.inputData(Locators.email, Labels.emailTest);
        elements.inputData(Locators.phone, Labels.phoneTest);
        elements.inputData(Locators.password, Labels.emptyField);
        elements.inputData(Locators.city, Labels.cityTestRegistered);
        elements.clickOnElement(Locators.checkbox);
        elements.clickOnElement(Locators.registrationButton);
    }

    public void passwordFieldCheck() {
        assertions.equalsOfText(elements.getElementText(Locators.fillInFieldXpath), Labels.fillInFieldText);
    }

    public void cityFieldTest(String input) {
        elements.inputData(Locators.name, Labels.nameTest);
        elements.inputData(Locators.email, Labels.emailTest);
        elements.inputData(Locators.phone, Labels.phoneTest);
        elements.inputData(Locators.password, Labels.passwordTest);
        elements.inputData(Locators.city, input);
        elements.clickOnElement(Locators.checkbox);
        elements.clickOnElement(Locators.registrationButton);
    }

    public void cityFieldCheck(String input) {
        if (Objects.equals(input, "")) {
            assertions.equalsOfText(elements.getElementText(Locators.fillInFieldXpath), Labels.fillInFieldText);
        } else {
            assertions.equalsOfText(elements.getElementText(Locators.startEnteringCityXpath), Labels.startEnteringCityText);
        }
    }

    public void checkboxTest() {
        elements.inputData(Locators.name, Labels.nameTest);
        elements.inputData(Locators.email, Labels.emailTest);
        elements.inputData(Locators.phone, Labels.phoneTest);
        elements.inputData(Locators.password, Labels.passwordTest);
        elements.inputData(Locators.city, Labels.cityTestRegistered);
        elements.clickOnElement(Locators.registrationButton);
    }

    public void checkboxCheck() {
        assertions.equalsOfText(elements.getElementText(Locators.markItXpath), Labels.markItText);
    }

    public void registeredUserTest() {
        elements.inputData(Locators.name, Labels.nameRegistered);
        elements.inputData(Locators.email, Labels.emailRegistered);
        elements.inputData(Locators.phone, Labels.phoneRegistered);
        elements.inputData(Locators.password, Labels.passwordRegistered);
        elements.inputData(Locators.city, Labels.cityTestRegistered);
        elements.clickOnElement(Locators.checkbox);
        elements.clickOnElement(Locators.registrationButton);
    }

    public void registeredUserCheck() {
        assertions.equalsOfText(elements.getElementText(Locators.userAlreadyExistsXpath), Labels.userAlreadyExistsText);
    }
}