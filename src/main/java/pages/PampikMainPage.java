package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.Objects;
import java.util.Set;

public class PampikMainPage extends BasePage {

    public PampikMainPage(WebDriver driver) {
        super(driver);
    }

    private static class Locators {
        private final static By logo = By.cssSelector(".js-logo");
        private final static By notifications = By.cssSelector(".icon.icon-alarm.notifications-link__icon");
        private final static By wishList = By.cssSelector(".icon.icon-d-heaet.fave-link__icon");
        private final static By checkOrder = By.cssSelector("#check-order");
        private final static By loginForm = By.cssSelector(".login-form__title");
        private final static By diapersAndChanging = By.xpath("//span[contains(text(), 'Підгузки і сповивання')]");
        private final static By searchFormInput = By.cssSelector("#search-form__input");
        private final static By searchResults = By.xpath("//div/h1");
        private final static By nothingFound = By.xpath("//mark");
        private final static By login = By.xpath("//span[text()='Увійти']");
        private final static By loginPhone = By.cssSelector("[name='Customer[login_phone]']");
        private final static By customerPassword = By.cssSelector("[name='Customer[customer_password]']");
        private final static By submitLogin = By.cssSelector("#submit-login");
        private final static By errorMsg = By.cssSelector(".form__row>div");
    }

    private static class Labels {
        private final static String url = "https://pampik.com/ua";
        private final static String pageTitle = "PAMPIK - інтернет-магазин дитячих товарів | Товари для дітей та батьків з доставкою по Києву та Україні";
        private final static String loginFormText = "Вхід до особистого кабінету";
        private final static String spanXpathTextContains = "//span[contains(text(), '";
        private final static String h1XpathTextContains = "//h1[contains(text(), '";
        private final static String xpathEnding = "')]";
        private final static String aXpathTextContains = "//a[contains(text(), '";
        private final static String searchResultsOnRequest = "Результати пошуку за запитом ";
        private final static String weCouldNotFindAnything = "Ми не змогли нічого знайти за запитом:";
        private final static String fillInFieldText = "Заповніть поле";
        private final static String InvalidLoginOrPassword = "Невірний логін або пароль";
    }

    public void openPage() {
        driver.get(Labels.url);
    }

    public void ctrlClickOnLogoAndSwitchToNewWindow() {
        Set<String> set1 = driver.getWindowHandles();
        action.CtrlClickElement(elements.findElement(Locators.logo));
        Set<String> set2 = driver.getWindowHandles();
        set2.removeAll(set1);
        elements.switchToWindow(set2.iterator().next());
    }

    public void checkPageTitle() {
        assertions.equalsOfText(elements.getTitleText(), Labels.pageTitle);
    }

    public void clickNotificationsButton() {
        elements.clickOnElement(Locators.notifications);
    }

    public void clickWishListButton() {
        elements.clickOnElement(Locators.wishList);
    }

    public void clickCheckOrderButton() {
        elements.clickOnElement(Locators.checkOrder);
    }

    public void checkLoginForm() {
        assertions.equalsOfText(elements.getElementText(Locators.loginForm), Labels.loginFormText);
    }

    public void clickMainMenuButtonByXpath(String text) {
        elements.clickOnElementByXpath(Labels.spanXpathTextContains + text + Labels.xpathEnding);
    }

    public void checkPageTitleByXpath(String text) {
        assertions.equalsOfTextByXpath(Labels.h1XpathTextContains + text + Labels.xpathEnding, text);
    }

    public void clickDiapersAndChangingSubmenuButtonByXpath(String text) {
        elements.waitSomeTime(500);
        action.moveToElement(elements.findElement(Locators.diapersAndChanging));
        elements.clickOnElementByXpath(Labels.aXpathTextContains + text + Labels.xpathEnding);
    }

    public void searchFieldInput(String input) {
        elements.inputDataThenEnter(Locators.searchFormInput, input);
    }

    public void searchResultCheck(String input) {
        if (elements.findElement(Locators.searchResults).getText().contains(Labels.searchResultsOnRequest)) {
            assertions.equalsOfText(elements.getElementText(Locators.searchResults).replace(Labels.searchResultsOnRequest, ""), "\"" + input + "\"");
        }
        if (elements.findElement(Locators.searchResults).getText().contains(Labels.weCouldNotFindAnything)) {
            assertions.equalsOfText(elements.getElementText(Locators.nothingFound), "«" + input + "»");
        }
    }

    public void clickLoginButton() {
        elements.clickOnElement(Locators.login);
    }

    public void accountLoginTest(String firstText, String secondText) {
        elements.inputData(Locators.loginPhone, firstText);
        elements.inputData(Locators.customerPassword, secondText);
        elements.clickOnElement(Locators.submitLogin);
    }

    public void accountLoginCheck(String firstText, String secondText) {
        if (Objects.equals(firstText, "") || Objects.equals(secondText, "")) {
            assertions.equalsOfText(elements.getElementText(Locators.errorMsg), Labels.fillInFieldText);
        } else {
            assertions.equalsOfText(elements.getElementText(Locators.errorMsg), Labels.InvalidLoginOrPassword);
        }
    }
}