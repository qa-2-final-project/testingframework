import base.BaseTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class PampikRegisterPageTest extends BaseTest {
    @Test(dataProvider = "NameProvider")
    public void nameFieldEntry(String input) {
        pampikRegisterPage.openPage();
        pampikRegisterPage.nameFieldTest(input);
        pampikRegisterPage.nameFieldCheck(input);
    }

    @DataProvider(name = "NameProvider")
    public Object[][] nameText() {
        return new Object[][]{{""}, {"Василь 12345678"}};
    }

    @Test(dataProvider = "EmailProvider")
    public void emailFieldEntry(String input) {
        pampikRegisterPage.openPage();
        pampikRegisterPage.emailFieldTest(input);
        pampikRegisterPage.emailFieldCheck();
    }

    @DataProvider(name = "EmailProvider")
    public Object[][] emailText() {
        return new Object[][]{{""}, {"vasyl.testenko.com"}};
    }

    @Test(dataProvider = "PhoneProvider")
    public void phoneFieldEntry(String input) {
        pampikRegisterPage.openPage();
        pampikRegisterPage.phoneFieldTest(input);
        pampikRegisterPage.phoneFieldCheck(input);
    }

    @DataProvider(name = "PhoneProvider")
    public Object[][] phoneText() {
        return new Object[][]{{""}, {"101010101"}};
    }

    @Test
    public void passwordFieldEntry() {
        pampikRegisterPage.openPage();
        pampikRegisterPage.passwordFieldTest();
        pampikRegisterPage.passwordFieldCheck();
    }

    @Test(dataProvider = "CityProvider")
    public void cityFieldEntry(String input) {
        pampikRegisterPage.openPage();
        pampikRegisterPage.cityFieldTest(input);
        pampikRegisterPage.cityFieldCheck(input);
    }

    @DataProvider(name = "CityProvider")
    public Object[][] cityText() {
        return new Object[][]{{""}, {"Тестопіль"}};
    }

    @Test
    public void checkboxIsNotChecked() {
        pampikRegisterPage.openPage();
        pampikRegisterPage.checkboxTest();
        pampikRegisterPage.checkboxCheck();
    }

    @Test
    public void registeredUserEntry() {
        pampikRegisterPage.openPage();
        pampikRegisterPage.registeredUserTest();
        pampikRegisterPage.registeredUserCheck();
    }
}