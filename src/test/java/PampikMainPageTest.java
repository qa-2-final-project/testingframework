import base.BaseTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class PampikMainPageTest extends BaseTest {

    @Test
    public void ctrlClickTheCompanyLogo() {
        pampikMainPage.openPage();
        pampikMainPage.ctrlClickOnLogoAndSwitchToNewWindow();
        pampikMainPage.checkPageTitle();
    }

    @Test
    public void clickNotificationsWishListCheckOrderButtons() {
        pampikMainPage.openPage();
        pampikMainPage.clickNotificationsButton();
        pampikMainPage.checkLoginForm();
        pampikMainPage.clickWishListButton();
        pampikMainPage.checkLoginForm();
        pampikMainPage.clickCheckOrderButton();
        pampikMainPage.checkLoginForm();
    }

    @Test(dataProvider = "mainMenuButtons")
    public void clickMainMenuButtons(String firstText, String secondText) {
        pampikMainPage.openPage();
        pampikMainPage.clickMainMenuButtonByXpath(firstText);
        pampikMainPage.checkPageTitleByXpath(secondText);
    }

    @DataProvider(name = "mainMenuButtons")
    public Object[][] buttonsAndPageTitlesText() {
        return new Object[][]{{"Підгузки і сповивання", "Підгузки"},
                {"Харчування і годування", "Дитяче харчування і товари для годування"},
                {"Іграшки та творчість", "Іграшки та творчість"},
                {"Догляд, купання і гігієна", "Догляд та купання"},
                {"Вагітність і материнство", "Материнство"},
                {"Дитяча кімната і безпека", "Дитяча кімната"},
                {"Прогулянки і поїздки", "Прогулянки і поїздки"},
                {"Для дому та сім", "Побутова хімія та косметика"},
                {"Зоотовари", "Зоотовари"},
                {"Одяг та взуття", "Дитячий одяг та взуття"}};
    }

    @Test(dataProvider = "diapersAndChangingSubmenuButtons")
    public void clickSubmenuButtons(String firstText, String secondText) {
        pampikMainPage.openPage();
        pampikMainPage.clickDiapersAndChangingSubmenuButtonByXpath(firstText);
        pampikMainPage.checkPageTitleByXpath(secondText);
    }

    @DataProvider(name = "diapersAndChangingSubmenuButtons")
    public Object[][] submenuButtonsAndTitles() {
        return new Object[][]{{"Одноразові підгузки", "Підгузники (памперси)"},
                {"Японські підгузки", "Японські підгузки"},
                {"Екопідгузки", "Екопідгузки"},
                {"Багаторазові підгузки", "Багаторазові підгузники"},
                {"Вологі серветки", "Вологі серветки"},
                {"Присипки і креми під підгузок", "Присипки і креми під підгузок"},
                {"Одноразові пелюшки", "Одноразові пелюшки"},
                {"Багаторазові пелюшки", "Багаторазові пелюшки"},
                {"Пеленатори і матрацики для сповивання", "Пеленатори та пеленальні матраци"},
                {"Пеленальні столики й комоди", "Сповивальні столики й комоди"},
                {"Підгузки поштучно", "Підгузки поштучно"},
                {"Накопичувачі для підгузків", "Накопичувачі для підгузків"},
                {"Привчання до горщика", "Привчання до горщика"}};
    }

    @Test(dataProvider = "SearchProvider")
    public void searchFieldEntry(String input) {
        pampikMainPage.openPage();
        pampikMainPage.searchFieldInput(input);
        pampikMainPage.searchResultCheck(input);
    }

    @DataProvider(name = "SearchProvider")
    public Object[][] searchText() {
        return new Object[][]{{"машина"}, {"input"}, {"сенс"}};
    }

    @Test(dataProvider = "accountProvider")
    public void accountLoginWithInvalidData(String firstText, String secondText) {
        pampikMainPage.openPage();
        pampikMainPage.clickLoginButton();
        pampikMainPage.accountLoginTest(firstText, secondText);
        pampikMainPage.accountLoginCheck(firstText, secondText);
    }

    @DataProvider(name = "accountProvider")
    public Object[][] accountInvalidData() {
        return new Object[][]{{"", ""}, {"101010101", "101010101"}};
    }
}