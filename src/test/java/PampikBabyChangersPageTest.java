import base.BaseTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class PampikBabyChangersPageTest extends BaseTest {
    @BeforeClass
    public void registeredUserLogin() {
        pampikBabyChangersPage.openPage();
        pampikBabyChangersPage.clickLoginButton();
        pampikBabyChangersPage.accountLogin();
    }

    @Test
    public void CheckingWishlistButtonCounter() {
        pampikBabyChangersPage.clickDiapersAndChangingSubmenuBabyChangersButtonByXpath();
        pampikBabyChangersPage.sortingByName();
        pampikBabyChangersPage.moveToItemAndClickWishlistButton();
        pampikBabyChangersPage.windowScrollToWishListCounter();
        pampikBabyChangersPage.wishlistCounterCheck();
        pampikBabyChangersPage.clickOnWishListButton();
        pampikBabyChangersPage.deleteItemFromWishlist();
    }

    @Test
    public void CheckingWishlistPage() {
        pampikBabyChangersPage.clickDiapersAndChangingSubmenuBabyChangersButtonByXpath();
        pampikBabyChangersPage.sortingByName();
        pampikBabyChangersPage.moveToItemAndClickWishlistButton();
        pampikBabyChangersPage.windowScrollToWishListCounter();
        pampikBabyChangersPage.clickOnWishListButton();
        pampikBabyChangersPage.wishlistPageCheck();
        pampikBabyChangersPage.deleteItemFromWishlist();
    }

    @Test
    public void CheckingCartButtonCounter() {
        pampikBabyChangersPage.clickDiapersAndChangingSubmenuBabyChangersButtonByXpath();
        pampikBabyChangersPage.sortingByName();
        pampikBabyChangersPage.moveToItemAndClickCartButton();
        pampikBabyChangersPage.windowScrollToCartCounter();
        pampikBabyChangersPage.cartCounterCheck();
        pampikBabyChangersPage.moveToCartButtonAndRemoveAllItems();
    }

    @Test
    public void CheckingCartPage() {
        pampikBabyChangersPage.clickDiapersAndChangingSubmenuBabyChangersButtonByXpath();
        pampikBabyChangersPage.sortingByName();
        pampikBabyChangersPage.moveToItemAndClickCartButton();
        pampikBabyChangersPage.windowScrollToCartCounter();
        pampikBabyChangersPage.moveToCartButton();
        pampikBabyChangersPage.cartPageCheck();
        pampikBabyChangersPage.moveToCartButtonAndRemoveAllItems();
    }

    @Test
    public void CheckingMoveAllItemsToWishlistAndClear() {
        pampikBabyChangersPage.clickDiapersAndChangingSubmenuBabyChangersButtonByXpath();
        pampikBabyChangersPage.sortingByName();
        pampikBabyChangersPage.moveToItemAndClickCartButton();
        pampikBabyChangersPage.windowScrollToCartCounter();
        pampikBabyChangersPage.moveToCartButtonAndMoveAllItemsToWishlistAndClear();
        pampikBabyChangersPage.windowScrollToWishListCounter();
        pampikBabyChangersPage.wishlistCounterCheck();
        pampikBabyChangersPage.clickOnWishListButton();
        pampikBabyChangersPage.deleteItemFromWishlist();
    }

    @Test
    public void CheckingOrderPage() {
        pampikBabyChangersPage.clickDiapersAndChangingSubmenuBabyChangersButtonByXpath();
        pampikBabyChangersPage.sortingByName();
        pampikBabyChangersPage.moveToItemAndClickCartButton();
        pampikBabyChangersPage.windowScrollToCartCounter();
        pampikBabyChangersPage.clickMakeOut();
        pampikBabyChangersPage.clickPlaceAnOrder();
        pampikBabyChangersPage.orderPageCheck();
        pampikBabyChangersPage.clickEditOrder();
        pampikBabyChangersPage.clickRemoveOrder();
        pampikBabyChangersPage.clickBackToShopping();
    }
}